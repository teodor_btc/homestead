require 'json'
require 'yaml'

VAGRANTFILE_API_VERSION = "2"
confDir = $confDir ||= File.expand_path("~/.homestead")

homesteadYamlPath = confDir + "/Homestead.yaml"
homesteadJsonPath = confDir + "/Homestead.json"
afterScriptPath = confDir + "/after.sh"
aliasesPath = confDir + "/aliases"

# database backup and import
databaseBackupAndImport = File.expand_path(File.dirname(__FILE__) + '/scripts/databaseBackupAndImport_import.sh')

require File.expand_path(File.dirname(__FILE__) + '/scripts/homestead.rb')

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    if File.exists? aliasesPath then
        config.vm.provision "file", source: aliasesPath, destination: "~/.bash_aliases"
    end

    if File.exists? homesteadYamlPath then
        Homestead.configure(config, YAML::load(File.read(homesteadYamlPath)))
    elsif File.exists? homesteadJsonPath then
        Homestead.configure(config, JSON.parse(File.read(homesteadJsonPath)))
    end
	
	# Run database backup and import before after.sh
	if File.exists? databaseBackupAndImport then
		config.vm.provision "shell", path: databaseBackupAndImport
	end
	
    if File.exists? afterScriptPath then
        config.vm.provision "shell", path: afterScriptPath
    end
    
    # hostmanager
    config.ssh.forward_agent = true
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
end
