# Laravel Homestead

The official Laravel local development environment.

Official documentation [is located here](http://laravel.com/docs/5.1/homestead).

# Install

## Required Software

### 1. VirtualBox

* Download and install https://www.virtualbox.org/wiki/Downloads
* Change box location: File -> Preferences -> General -> Default Machine Folder (**VM_PATH**)

### 2. Vagrant

* Download and install https://www.vagrantup.com/downloads.html

### 3. Cygwin

* Download and install https://cygwin.com/install.html with: openssh, git, git-svn
* Shortcut Target:
```
#!sh


"CYGWIN_PATH\bin\mintty.exe -i /Cygwin-Terminal.ico -"
```
* Right Click on shortcut -> Compatibility -> Check "Run this program as an administrator"
* Run and close
* Open:
```
#!sh


CYGWIN_PATH\home\{USER}\.bash_profile"
```
and add:
```
#!sh


CYGWIN_INSTALL_PATH=`cygpath -w -p /`
eval `ssh-agent`
ssh-add
export VAGRANT_DETECTED_OS=cygwin
export VAGRANT_HOME=${CYGWIN_INSTALL_PATH}${HOME}"/.vagrant.d"
```
	
* Open:
```
#!sh


CYGWIN_PATH\home\{USER}\.bashrc
```
and add:
```
#!sh


alias vm="ssh vagrant@127.0.0.1 -p 2222"
```
		
* Make a new file:
```
#!sh


CYGWIN_PATH\home\{USER}\.bash_logout
```
and add:
```
#!sh


eval `ssh-agent -k`
```
	
8. Open CYGWIN and run commands

```
#!sh


ssh-keygen -t rsa -b 2048 -C "you@homestead"
vagrant box add laravel/homestead
vagrant plugin install vagrant-hostmanager
vagrant plugin install vagrant-triggers
git clone https://teodor_btc@bitbucket.org/teodor_btc/homestead.git Homestead
cd Homestead/ && bash init.sh
vagrant up
```
	
### 4. (Optional) PhpMyAdmin

* Download [phpMyAdmin-x.x.x.x-english.zip](https://www.phpmyadmin.net/downloads/)
* Unzip and copy the content to 
```
#!sh


CODE_PATH\_tools\phpmyadmin
```

## PATHS

```
#!sh


CYGWIN_PATH = cygwin install location (M:\_server)
VM_PATH = virtualbox box location (M:\VMS)
CODE_PATH = code location (M\_code)
```

## PASSWORDS

MySQL User and Pass
```
#!sh


root:secret
homestead:secret
```