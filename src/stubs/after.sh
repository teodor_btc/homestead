#!/usr/bin/env bash

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.
function configure_nginx_sites {
	echo "* Copying default nginx conf"
    sudo cp -a /home/vagrant/Code/_server/etc/nginx/sites-available/. /etc/nginx/sites-available/
    #sudo ln -s /etc/nginx/sites-available/warp.dev /etc/nginx/sites-enabled/warp.dev
    sudo service nginx restart
}
configure_nginx_sites